//require liberallys and paths
var express = require('express');
var router = express.Router();
var Busboy = require('busboy');
var mysql = require('mysql');
var fs = require('fs');
const { exec } = require('child_process');
const { post } = require('../app');
const { route } = require('./users');

//data pase connection
var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "raspi",
  database: "steam"
});




// sign up function 
router.post('/register', function (req, res, next) {
  //if missing email or password
  if (req.body.email && req.body.pass) {
    var email = req.body.email
    var pass = req.body.pass
    //serash by email in database 
    var sql = "SELECT * FROM users WHERE email = ?";
    con.query(sql, email, function (err, result, fields) {
      if (err) throw err;
      //if email exist 
      if (result.length == !"0") {
        //insert email and password in database
        var sql = "INSERT INTO `users` (`email`, `pass`) VALUES ('" + email + "','" + pass + "')";
        con.query(sql, function (err, result) {
          if (err) throw err;
          // send resbonse sign up sucsess
          return res.send("email_sign_up_sucsess")
        });
      }else{
        // send resbonse email is exist
        return res.send("email_exist")
      }
    })
  }else {
       // send resbonse email is exist
        return res.send("missing_data")
  }
});







//login function 
router.post('/login', function (req, res, next) {
  //if missing data
  if (req.body.email && req.body.pass) {
    var email = req.body.email
    var pass = req.body.pass
    //search in database email and password 
    var sql = "SELECT * FROM users WHERE email = ?";
    con.query(sql, email, function (err, result, fields) {
      if (err) throw err;
      if (result.length) {
        if (pass == result[0].pass) {
          // if email and password true send response to mobile app 
          return res.send("login_sucsess")
        } else {
         //if password wrong send response to mobile app 
          return res.send("email_dons't_exist")
        }
      } else {
        //if password or email wrong 
        return res.send("wrong_email_or_password")
      }
    })
  }
  else {
   //if password wrong send response to mobile app 
    return res.send("missing_email_or_password")
  }
});


// recive request to save image
router.post('/fileupload', function (req, res, next) {
  var busboy = new Busboy({ headers: req.headers });
  busboy.on('file', function (fieldname, file, filename, encoding, mimetype) {
    if (filename) {
      var saveTo = "uploads/" + filename + ".png"
      // save image on folder name ubloads
      file.pipe(fs.createWriteStream(saveTo));
      busboy.on("finish", () => {
        //if ubload finish send response to mobile app 
        res.send(JSON.parse('{ "upload":"yes" }'))
      });
    } else {
      //if ubload fail send response to mobile app 
      return res.send(JSON.parse('{ "upload":"no" }'))
    }
  });
  req.pipe(busboy)
})


// recive request to run 
router.post('/ml_scan', function (req, res, next) {
  //ecxute command to runn python script
  exec('python3 mlscan.py uploads/' + req.body.email + '.png', (error, re, stderr) => {
    if (error) {
      return;
    }
    var data = re.split('\n')
    var d = data[0]
    if (d == 1) {
      //if if have canser send response to mobile app 
      res.send(JSON.parse('{ "canser":"yes" }'))
      console.log("canser :yes" )
    }
    else if (d == 0) {
      //if if dosen't have canser send response to mobile app 
      res.send(JSON.parse('{ "canser":"no" }'))
      console.log("canser :no" )

    }
  });
})



module.exports = router;
