
import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)
import math
import cv2
import matplotlib.pyplot as plt
import sys
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3' 
import seaborn as sns
import umap
from PIL import Image
from scipy import misc
from os import listdir
from os.path import isfile, join
import numpy as np
from scipy import misc
from random import shuffle
from collections import Counter
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
import tensorflow as tf
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras.utils.np_utils import to_categorical
from sklearn.ensemble import RandomForestClassifier
from sklearn import datasets
import joblib

os.listdir('input/brain_tumor_dataset')

yes=os.listdir('input/brain_tumor_dataset/yes')
no=os.listdir('input/brain_tumor_dataset/no')
data=np.concatenate([yes,no])
len(data)==len(yes)+len(no)
target_x=np.full(len(yes),1)
target_y=np.full(len(no),0)
data_target=np.concatenate([target_x,target_y])
len(data_target)==len(target_x)+len(target_y)
len(data_target)==len(data)
data_target
data
yes_values=os.listdir('input/brain_tumor_dataset/yes')
no_values=os.listdir('input/brain_tumor_dataset/no')
X_data =[]

img = cv2.imread(sys.argv[1])
face = cv2.resize(img, (32, 32) )
(b, g, r)=cv2.split(face) 
img=cv2.merge([r,g,b])
X_data.append(img)
X_data.append(img)
X_data.append(img)

X = np.squeeze(X_data)
X.shape
X = X.astype('float32')
X /= 255
(x_test, y_test) =  (X[2:] , data_target[2:])

model = tf.keras.models.load_model('brain_model')

y_hat = model.predict(x_test)

print(round(y_hat[0][0]))

